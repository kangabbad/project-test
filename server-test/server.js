const express = require('express')
const app = express()
const cors = require('cors')
const Joi = require('joi')

const PORT = process.env.PORT || 3001;

app.use(cors())
app.use(express.json())

app.listen(PORT, () => {
  console.log(`Server is listening on ${PORT}`);
})

let random = []
let proportional = []
let custom = []

app.get('/', (req, res) => {
  res.send('halo react')
})

app.get('/random/', (req, res) => {
  res.send(random)
})

app.post('/random/', (req, res) => {
  const getRandomArbitrary = (min, max) => {
    const randomMath =  Math.random() * (max - min) + min
    return Math.round(randomMath)
  }

  const schema = {
    noQuestion: Joi.number().integer().min(1).required(),
    noRespondent: Joi.number().integer().min(10).required()
  }

  const validator = Joi.validate(req.body, schema)
  console.log(validator)

  if (validator.error) {
    res.status(400).send(validator.error.details[0].message)
    return
  }

  const randomCalc = {
    priceLevel: 1,
    noQuestion: parseFloat(req.body.noQuestion),
    noRespondent: parseFloat(req.body.noRespondent),
    target: 'Random by system',
    availableRespondent: getRandomArbitrary(1000, 30000),
    result: req.body.noQuestion * req.body.noRespondent * 1000
  }

  random.push(randomCalc)
  res.send(randomCalc)
})

app.get('/proportional/', (req, res) => {
  res.send(proportional)
})

app.post('/proportional/', (req, res) => {
  const getProportionalArbitrary = (min, max) => {
    const randomMath =  Math.random() * (max - min) + min
    return Math.round(randomMath)
  }

  const schema = {
    noQuestion: Joi.number().integer().min(1).required(),
    noRespondent: Joi.number().integer().min(10).required()
  }

  const validator = Joi.validate(req.body, schema)
  console.log(validator)

  if (validator.error) {
    res.status(400).send(validator.error.details[0].message)
    return
  }

  const proportionalCalc = {
    priceLevel: 1.4,
    noQuestion: parseFloat(req.body.noQuestion),
    noRespondent: parseFloat(req.body.noRespondent),
    target: 'Jawa Tengah, Indonesia',
    availableRespondent: getProportionalArbitrary(1000, 30000),
    result: Math.round(req.body.noQuestion * req.body.noRespondent * 1000 * 1.4)
  }

  proportional.push(proportionalCalc)
  res.send(proportionalCalc)
})

app.get('/custom/', (req, res) => {
  res.send(custom)
})

app.post('/custom/', (req, res) => {
  const getCustomArbitrary = (min, max) => {
    const randomMath =  Math.random() * (max - min) + min
    return Math.round(randomMath)
  }

  const schema = {
    noQuestion: Joi.number().integer().min(1).required(),
    noRespondent: Joi.number().integer().min(10).required()
  }

  const validator = Joi.validate(req.body, schema)
  console.log(validator)

  if (validator.error) {
    res.status(400).send(validator.error.details[0].message)
    return
  }

  const customCalc = {
    priceLevel: 2,
    noQuestion: parseFloat(req.body.noQuestion),
    noRespondent: parseFloat(req.body.noRespondent),
    // target: {
    //   basicDemography: {
    //     gender: req.body.gender,
    //     age: {
    //       from: req.body.from,
    //       to: req.body.to
    //     },
    //     location: req.body.location
    //   },
    //   mobileOnlineHabits: {
    //     phoneOs: req.body.phoneOS,
    //     operator: req.body.phoneOS,
    //     installedApps: req.body.installedApps,
    //   }
    // },
    availableRespondent: getCustomArbitrary(1000, 30000),
    result: Math.round(req.body.noQuestion * req.body.noRespondent * 1000 * 2)
  }

  custom.push(customCalc)
  res.send(customCalc)
})