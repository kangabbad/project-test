import React, { Component } from 'react'
import { BrowserRouter as Router } from 'react-router-dom'

import Aux from './component/Aux/Auxiliary'
import Option from './component/Option/Option'
import Content from './component/Content/Content'

class App extends Component {
  render () {
    return (
      <Router>
        <Aux>
          <Option />
          <Content />
        </Aux>
      </Router>
    )
  }
}

export default App
