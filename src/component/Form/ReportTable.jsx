import React from 'react'
import PropTypes from 'prop-types'
import { Table, TableHead, TableBody } from 'mdbreact'
import Aux from './../Aux/Auxiliary'

const ReportTable = props => {
  return (
    <Aux>
      {props.dataTable.map((datum, index) => (
        <Table responsive id={props.tableId} key={index}>
          <TableHead color='primary-color' textWhite>
            <tr>
              {datum.tableTitle.map((title, index) => (
                <th key={index}>{title}</th>
              ))}
            </tr>
          </TableHead>
          <TableBody>
            <tr>
              {datum.tableData.map((data, i) => (
                <td key={i}>{data}</td>
              ))}
            </tr>
          </TableBody>
        </Table>
      ))}
    </Aux>
  )
}

ReportTable.propTypes = {
  tableId: PropTypes.string,
  dataTable: PropTypes.array
}

export default ReportTable
