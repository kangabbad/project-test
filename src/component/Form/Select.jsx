import React from 'react'
import PropTypes from 'prop-types'
import { FormGroup, Label, CustomInput } from 'reactstrap'

const Select = props => {
  return (
    <FormGroup>
      <Label for={props.for}>{props.label}</Label>
      <CustomInput type='select' id={props.for} name={props.for}>
        <option value=''>Select</option>
        {props.options.map((datum, index) => {
          return (
            <option key={index}>{datum}</option>
          )
        })}
      </CustomInput>
    </FormGroup>
  )
}

Select.propTypes = {
  for: PropTypes.string,
  label: PropTypes.string,
  options: PropTypes.array
}

export default Select
