import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { Row, Col } from 'mdbreact'
import Aux from './../Aux/Auxiliary'

class BetweenInput extends Component {
  constructor (props) {
    super(props)
    this.state = {
      from: '',
      to: ''
    }

    this.handleChange = this.handleChange.bind(this)
  }

  handleChange (event) {
    const num = event.target.value
    if (event.target.validity.valid) {
      this.setState({
        [event.target.name]: event.target.value
      })
    } else if (num === '' || num === '-') {
      this.setState({
        [event.target.name]: num
      })
    }
  }

  render () {
    return (
      <Aux>
        <label htmlFor={this.props.for}>{this.props.label}</label>
        <Row>
          <Col md='6'>
            <div className='md-form input-group my-2 my-md-0'>
              <div className='input-group-prepend'>
                <span className='input-group-text rounded-0 grey lighten-5'>from</span>
              </div>
              <input
                name='from'
                type='text'
                className='form-control'
                pattern='^-?[0-9]\d*\.?\d*$'
                onChange={this.handleChange}
                value={this.state.from}
                placeholder='years old'
              />
            </div>
          </Col>
          <Col md='6'>
            <div className='md-form input-group my-2 my-md-0 py-2 py-md-0'>
              <div className='input-group-prepend'>
                <span className='input-group-text rounded-0 grey lighten-5'>to</span>
              </div>
              <input
                name='to'
                type='text'
                className='form-control'
                pattern='^-?[0-9]\d*\.?\d*$'
                onChange={this.handleChange}
                value={this.state.to}
                placeholder='years old'
              />
            </div>
          </Col>
        </Row>
      </Aux>
    )
  }
}

BetweenInput.propTypes = {
  for: PropTypes.string,
  label: PropTypes.string
}

export { BetweenInput }
