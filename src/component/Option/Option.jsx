import React, { Component } from 'react'
import { Link } from 'react-router-dom'

import { Container, Row, Col, Button } from 'mdbreact'

class Option extends Component {
  render () {
    return (
      <header id='nav-menu'>
        <div className='jumbotron jumbotron-fluid blue accent-4 text-white text-center m-0'>
          <Container>
            <h1 className='display-4 text-capitalize font-weight-normal'>lets survey your perception now</h1>
            <p className='lead font-italic'>ask before do things.</p>
          </Container>
        </div>
        <div className='menu-option py-4 blue lighten-1'>
          <Container>
            <Row>
              <Col sm='4'>
                <Link to='/'>
                  <Button className='blue accent-4 w-100 m-0 my-2 px-0'>Random</Button>
                </Link>
              </Col>
              <Col sm='4'>
                <Link to='/proportional'>
                  <Button className='blue accent-4 w-100 m-0 my-2 px-0'>Proportional</Button>
                </Link>
              </Col>
              <Col sm='4'>
                <Link to='/custom'>
                  <Button className='blue accent-4 w-100 m-0 my-2 px-0'>Custom</Button>
                </Link>
              </Col>
            </Row>
          </Container>
        </div>
      </header>
    )
  }
}

export default Option
