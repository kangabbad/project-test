import React, { Component } from 'react'
import { Route } from 'react-router-dom'

import Aux from './../Aux/Auxiliary'
import Routes from './../Routes/Routes'

class Content extends Component {
  render () {
    return (
      <Aux>
        {/* {Routes.map((route, i) => <RouteWithSubRoutes key={i} {...route} />)} */}
        {Routes.map((route, index) => (
          <Route
            key={index}
            path={route.path}
            exact={route.exact}
            component={route.component}
          />
        ))}
      </Aux>
    )
  }
}

export default Content
