import Random from './../../content/Random/Random'
import Proportional from './../../content/Proportional/Proportional'
import Custom from './../../content/Custom/Custom'

const Routes = [
  {
    path: '/',
    exact: true,
    component: Random
  },
  {
    path: '/proportional',
    component: Proportional
  },
  {
    path: '/custom',
    component: Custom
  }
]

export default Routes
