import BasicDemography from '../../content/Custom/BasicDemography/BasicDemography'
import MobileOnlineHabits from '../../content/Custom/MobileOnlineHabits/MobileOnlineHabits'

const RoutesTargetMenu = [
  {
    path: '/custom',
    exact: true,
    component: BasicDemography
  },
  {
    path: '/custom/mobile-and-online-habits',
    component: MobileOnlineHabits
  }
]

export default RoutesTargetMenu
