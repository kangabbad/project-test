import React, { Component } from 'react'

import { ListGroup } from 'mdbreact'
import SideMenu from '../SideMenu/SideMenu'

class TargetMenu extends Component {
  render () {
    return (
      <section className='targeting-profile_menu'>
        <ListGroup>
          <SideMenu menuName='Basic Demography' linked='/custom' />
          <SideMenu menuName='Mobile and Online Habits' linked='/custom/mobile-and-online-habits' />
        </ListGroup>
      </section>
    )
  }
}

export default TargetMenu
