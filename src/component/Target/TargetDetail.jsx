import React, { Component } from 'react'
import { Route } from 'react-router-dom'

import Aux from './../Aux/Auxiliary'
import RoutesTargetMenu from '../Routes/RoutesTargetMenu'

class TargetDetail extends Component {
  render () {
    return (
      <Aux>
        {RoutesTargetMenu.map((route, index) => (
          <Route
            key={index}
            path={route.path}
            exact={route.exact}
            component={route.component}
          />
        ))}
      </Aux>
    )
  }
}

export default TargetDetail
