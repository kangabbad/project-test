import React, { Component } from 'react'
import { Link } from 'react-router-dom'

import Aux from './../Aux/Auxiliary'

class SideMenu extends Component {
  render () {
    return (
      <Aux>
        <Link
          to={this.props.linked}
          className='list-group-item list-group-item-action d-flex align-items-center px-3'
        >
          <span className='mx-auto'> {this.props.menuName} </span>
        </Link>
      </Aux>
    )
  };
}

export default SideMenu
