import React, { Component } from 'react'
import axios from 'axios'

import { Container, Row, Col, Input, Button, Collapse } from 'mdbreact'
import ReportTable from './../../component/Form/ReportTable'

class Proportional extends Component {
  constructor (props) {
    super(props)

    this.state = {
      priceLevel: '',
      noQuestion: '',
      noRespondent: '',
      target: '',
      availableRespondent: '',
      result: '',
      dataReport: [],
      calculateLoad: false,
      calculating: '',
      collapse: false
    }

    this.handleChange = this.handleChange.bind(this)
    this.submitHandler = this.submitHandler.bind(this)
    this.toggleReport = this.toggleReport.bind(this)
  }

  postData () {
    const postData = {
      noQuestion: this.state.noQuestion,
      noRespondent: this.state.noRespondent
    }

    axios.post('http://localhost:3001/proportional/', postData).then(res => {
      axios.get('http://localhost:3001/proportional/')
        .then(res => {
          this.setState({
            priceLevel: res.data[res.data.length - 1].priceLevel,
            noQuestion: res.data[res.data.length - 1].noQuestion,
            noRespondent: res.data[res.data.length - 1].noRespondent,
            target: res.data[res.data.length - 1].target,
            availableRespondent: res.data[res.data.length - 1].availableRespondent,
            result: res.data[res.data.length - 1].result,
            dataReport: [
              {
                tableTitle: [
                  'Questions',
                  'Respondent',
                  'Target',
                  'Available Respondent',
                  'Price Level',
                  'Price'
                ],
                tableData: [
                  res.data[res.data.length - 1].noQuestion,
                  res.data[res.data.length - 1].noRespondent,
                  res.data[res.data.length - 1].target,
                  res.data[res.data.length - 1].availableRespondent,
                  res.data[res.data.length - 1].priceLevel,
                  res.data[res.data.length - 1].result
                ]
              }
            ],
            calculateLoad: true
          })
        })
    })
  }

  submitHandler (event) {
    event.preventDefault()
    event.target.className += ' was-validated'
    this.postData()
    this.toggleReport()
  }

  handleChange (event) {
    const num = event.target.value
    if (event.target.validity.valid) {
      this.setState({
        [event.target.name]: event.target.value,
        calculating: 'calculating...'
      })
    } else if (num === '' || num === '-') {
      this.setState({
        [event.target.name]: num,
        calculating: ''
      })
    }
  }

  toggleReport () {
    this.setState({ collapse: true })
  }

  render () {
    return (
      <section id='proportional'>
        <form className='needs-validation' onSubmit={this.submitHandler} noValidate>
          <Container>
            <Row className='flex-column align-items-center'>
              <Col md='8'>
                <Row>
                  <Col md='6'>
                    <div className='grey-text'>
                      <Input
                        label='Number of Question'
                        name='noQuestion'
                        type='text'
                        className='is-valid'
                        pattern='^-?[0-9]\d*\.?\d*$'
                        onChange={this.handleChange}
                        value={this.state.noQuestion}
                        required
                      />
                    </div>
                  </Col>

                  <Col md='6'>
                    <div className='grey-text'>
                      <Input
                        label='Number of Respondent'
                        name='noRespondent'
                        type='text'
                        className='is-valid'
                        pattern='^-?[0-9]\d*\.?\d*$'
                        onChange={this.handleChange}
                        value={this.state.noRespondent}
                        required
                      />
                    </div>
                  </Col>
                </Row>
              </Col>
              <Col md='8'>
                <div className='text-center mb-3'>
                  <div className='custom-control custom-checkbox mb-4'>
                    <input
                      type='checkbox'
                      className='custom-control-input'
                      onChange={this.handleChange}
                      checked
                    />
                    <label className='custom-control-label' htmlFor='report'>Complete Report</label>
                  </div>

                  <Button size='sm' color='info' type='submit'>Estimate</Button>
                </div>

                <Row>
                  <Col md='6'>
                    <label htmlFor='price-level' className='disabled'>Price Level</label>
                    <input
                      id='price-level'
                      className='form-control text-center'
                      type='text'
                      value={1.4}
                      disabled
                    />
                  </Col>
                  <Col md='6'>
                    <label htmlFor='price' className='disabled'>Price (IDR)</label>
                    <input
                      type='text'
                      id='price'
                      className='form-control text-center'
                      value={this.state.calculateLoad ? this.state.result : this.state.calculating}
                      disabled
                    />
                  </Col>
                </Row>
              </Col>
            </Row>

            <Collapse isOpen={this.state.collapse}>
              <Row className='justify-content-center mt-5'>
                <Col sm='12'>
                  <ReportTable
                    tableId='random-report'
                    dataTable={this.state.dataReport}
                  />
                </Col>
              </Row>
            </Collapse>
          </Container>
        </form>
      </section>
    )
  }
}

export default Proportional
