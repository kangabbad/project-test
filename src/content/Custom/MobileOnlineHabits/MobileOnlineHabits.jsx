import React, { Component } from 'react'

import { Row, Col } from 'mdbreact'
import Select from '../../../component/Form/Select'

class MobileOnlineHabits extends Component {
  render () {
    return (
      <div className='targeting-profile_mobile'>
        <form>
          <h3>Mobile and Online Habits</h3>
          <Row className='mt-5'>
            <Col md='6'>
              <Select
                for='os'
                label='Phone OS'
                options={['iOS', 'Android', 'Windows', 'Symbian', 'Java']}
              />
            </Col>
            <Col md='6'>
              <Select
                for='operator'
                label='Operator'
                options={['XL Axiata', 'Telkomsel', 'Indosat', '3', 'Smartfren']}
              />
            </Col>
            <Col md='6'>
              <Select
                for='apps'
                label='Installed Apps'
                options={['9GAG', 'Instagram', 'Youtube', 'Go-Jek']}
              />
            </Col>
          </Row>
        </form>
      </div>
    )
  }
}

export default MobileOnlineHabits
