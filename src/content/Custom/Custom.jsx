import React, { Component } from 'react'
import axios from 'axios'
import { Container, Row, Col, Input, Button, Collapse } from 'mdbreact'

import TargetDetail from '../../component/Target/TargetDetail'
import TargetMenu from '../../component/Target/TargetMenu'
import ReportTable from '../../component/Form/ReportTable'

class Custom extends Component {
  constructor (props) {
    super(props)

    this.state = {
      priceLevel: '',
      noQuestion: '',
      noRespondent: '',
      target: {
        basicDemography: {
          gender: '',
          age: {
            from: '',
            to: ''
          },
          location: ''
        }
      },
      availableRespondent: '',
      result: '',
      dataReport: [],
      collapse: false
    }

    this.handleChange = this.handleChange.bind(this)
    this.submitHandler = this.submitHandler.bind(this)
    this.toggleReport = this.toggleReport.bind(this)
  }

  postData () {
    const postData = {
      noQuestion: this.state.noQuestion,
      noRespondent: this.state.noRespondent,
      fromChild: this.state.fromChild
    }

    axios.post('http://localhost:3001/custom/', postData).then(res => {
      axios.get('http://localhost:3001/custom/')
        .then(res => {
          this.setState({
            priceLevel: res.data[res.data.length - 1].priceLevel,
            noQuestion: res.data[res.data.length - 1].noQuestion,
            noRespondent: res.data[res.data.length - 1].noRespondent,
            // target: '???',
            availableRespondent: res.data[res.data.length - 1].availableRespondent,
            result: res.data[res.data.length - 1].result,
            dataReport: [
              {
                tableTitle: [
                  'Questions',
                  'Respondent',
                  'Target',
                  'Available Respondent',
                  'Price Level',
                  'Price'
                ],
                tableData: [
                  res.data[res.data.length - 1].noQuestion,
                  res.data[res.data.length - 1].noRespondent,
                  '???',
                  res.data[res.data.length - 1].availableRespondent,
                  res.data[res.data.length - 1].priceLevel,
                  res.data[res.data.length - 1].result
                ]
              }
            ]
          })
        })
    })
  }

  submitHandler (event) {
    event.preventDefault()
    event.target.className += ' was-validated'
    this.postData()
    this.toggleReport()
  }

  handleChange (event) {
    const num = event.target.value
    if (event.target.validity.valid) {
      this.setState({
        [event.target.name]: event.target.value
      })
    } else if (num === '' || num === '-') {
      this.setState({
        [event.target.name]: num
      })
    }
  }

  toggleReport () {
    this.setState({ collapse: true })
  }

  render () {
    return (
      <section id='custom'>
        <form className='needs-validation' onSubmit={this.submitHandler} noValidate>
          <Container>
            <Row className='flex-column align-items-center'>
              <Col md='8'>
                <Row>
                  <Col md='6'>
                    <div className='grey-text'>
                      <Input
                        label='Number of Question'
                        name='noQuestion'
                        type='text'
                        className='is-valid'
                        pattern='^-?[0-9]\d*\.?\d*$'
                        onChange={this.handleChange}
                        value={this.state.noQuestion}
                        required
                      />
                    </div>
                  </Col>

                  <Col md='6'>
                    <div className='grey-text'>
                      <Input
                        label='Number of Respondent'
                        name='noRespondent'
                        type='text'
                        className='is-valid'
                        pattern='^-?[0-9]\d*\.?\d*$'
                        onChange={this.handleChange}
                        value={this.state.noRespondent}
                        required
                      />
                    </div>
                  </Col>
                </Row>
              </Col>
            </Row>

            <div className='alert alert-info mt-4 rounded-0' role='alert'>
              You don't need to fill the gender box, if you want to target both male and female. !
            </div>

            <section className='targeting-profile'>
              <Row className='flex-column-reverse flex-md-row'>
                <Col md='9'>
                  <TargetDetail />
                </Col>
                <Col md='3'>
                  <TargetMenu />
                </Col>
              </Row>
            </section>

            <div className='text-center my-4'>
              <div className='custom-control custom-checkbox mb-4'>
                <input
                  type='checkbox'
                  className='custom-control-input'
                  onChange={this.handleChange}
                  checked
                />
                <label className='custom-control-label' htmlFor='report'>Complete Report</label>
              </div>

              <Button size='sm' color='info' type='submit'>Estimate</Button>
            </div>

            <Collapse isOpen={this.state.collapse}>
              <Row className='justify-content-center mt-5'>
                <Col sm='12'>
                  <ReportTable
                    tableId='random-report'
                    dataTable={this.state.dataReport}
                  />
                </Col>
              </Row>
            </Collapse>
          </Container>
        </form>
      </section>
    )
  }
}

export default Custom
