import React, { Component } from 'react'

import { Row, Col } from 'mdbreact'
import Select from '../../../component/Form/Select'
import { BetweenInput } from '../../../component/Form/Inputs'

class BasicDemography extends Component {
  render () {
    return (
      <div className='targeting-profile_basic'>
        <form>
          <h3>Basic Demography</h3>
          <Row className='mt-5'>
            <Col md='6'>
              <Select
                for='gender'
                label='Gender'
                options={['Male', 'Female']}
              />
            </Col>
            <Col md='6'>
              <BetweenInput
                for='age'
                label='Age'
              />
            </Col>
            <Col md='6'>
              <Select
                for='location'
                label='Location'
                options={['Surakarta', 'Jogjakarta', 'Magelang']}
              />
            </Col>
          </Row>
        </form>
      </div>
    )
  }
}

export default BasicDemography
